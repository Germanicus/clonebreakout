game.keyboard = {};

game.state= {

    pressedKeys:{
        left: false,
        right: false
    }
};

game.keyboard.getValue = function(key)
{

    switch(key)
    {

        case 'left':  return 37;
        case 'right': return 39;
        case 'enter': return 13;


    }
};


game.keyboard.KeyDown = function(event) {
    switch (event.keyCode) {

        case game.keyboard.getValue('left'):
            game.state.pressedKeys.left= true;

            break;

        case game.keyboard.getValue('right'):
           game.state.pressedKeys.right= true;
            break;
        case game.keyboard.getValue('enter'):
               game.states.play= true;
               game.states.start=false;
               game.states.end=false;
               break;
    }
};
game.keyboard.KeyUp = function(event) {
    switch (event.keyCode) {

        case game.keyboard.getValue('left'):
            game.state.pressedKeys.left= false;

            break;

        case game.keyboard.getValue('right'):
            game.state.pressedKeys.right= false;
            break;


    }
};
