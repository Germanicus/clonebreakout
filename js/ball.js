ball={};
ball.x=50;
ball.y=200;
ball.radius=10;
ball.dx=2;
ball.dy= -2;
ball.color= '#'+Math.floor(Math.random()*16777215).toString(16);
ball.draw= function()
{
    game.drawObjects('ball',ball.color, {x: ball.x, y:ball.y, radius: ball.radius});


};
ball.update= function()
{
    if(ball.x +ball.dx> game.canvas.width- ball.radius ||ball.x +ball.dx <ball.radius)
    {
        ball.dx= -ball.dx;
    }
    if(ball.y + ball.dy< ball.radius)
    {
        ball.dy = - ball.dy;
    }
    else if(ball.y + ball.dy> game.canvas.height - ball.radius )
    {
        if(ball.x> paddle.x && ball.x< paddle.x + paddle.width)
        {
            ball.dy = - ball.dy;
        }
        else {


            game.states.end=true;

        }
    }
    ball.x+=ball.dx;
    ball.y+=ball.dy;

};
