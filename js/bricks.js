bricks={};
bricks.column= 8;
bricks.row= 5;
bricks.width=75;
bricks.height= 20;
bricks.padding=10;
bricks.offsettop=30;
bricks.offsetleft=30;
bricks.color='#'+Math.floor(Math.random()*16777215).toString(16);
var sound=new Audio('sound/ping.wav');

var brick=[];

  for(var c=0;c<bricks.column; c++)
  {
      brick[c]=[];
      for(var r=0; r<bricks.row; r++)
      {



                brick[c][r] = {x: 0, y: 0, status: 1};



      }
  }

bricks.draw= function()
{


    for(var c=0; c<bricks.column; c++)
    {
        for(var r=0;r<bricks.row; r++) {

            if (brick[c][r].status == 1) {


                var brickx = (c * (bricks.width + bricks.padding)) + bricks.offsetleft;
                var bricky = (r * (bricks.height + bricks.padding)) + bricks.offsettop;



                brick[c][r].x = brickx;
                brick[c][r].y = bricky;

                game.drawObjects('rect', bricks.color, {
                    x: brickx,
                    y: bricky,
                    width: bricks.width,
                    height: bricks.height
                })
            }

        }

        }


};

bricks.update= function()
{
  for(var c=0; c<bricks.column; c++)
  {
      for(var r=0; r< bricks.row; r++)
      {
          var b= brick[c][r];
          if(brick[c][r].status==1)
          {
              if(ball.x> b.x  && ball.x < b.x + bricks.width && ball.y>b.y && ball.y< b.y +bricks.height)
              {

                  brick[c][r].status=0;
                  sound.play();
                  game.score++;
                
                  if(game.score== bricks.row*bricks.column)
                  {
                    game.states.win= true;
                  }
                  ball.dy= -ball.dy;



              }
          }
      }
  }
};
