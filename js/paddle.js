paddle={};
paddle.height= 10;
paddle.width= 75;
paddle.x=(game.canvas.width-paddle.width)/2;
paddle.color='0x'+Math.floor(Math.random()*16777215).toString(16);

paddle.draw= function () {

    game.drawObjects('rect',paddle.color, {x: paddle.x, y: game.canvas.height -paddle.height, width: paddle.width, height:paddle.height});

};
paddle.update= function()
{
    if(game.state.pressedKeys.right===true&& paddle.x<game.canvas.width-paddle.width)
    {
        paddle.x+=3;
    }
    if(game.state.pressedKeys.left===true && paddle.x>0)
    {
        paddle.x-=3;
    }

};
